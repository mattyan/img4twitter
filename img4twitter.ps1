<#
 写真をTwitter用に圧縮する
 要imagemagic
 @author mattyan
 @licence MIT
#>

# imagemagicのconvert.exeを探す
$path = ""
$identifyPath = ""
if (-not([string]::IsNullOrEmpty($Args[0])) -And (Test-Path $Args[0])) {
    # 引数にconvertへのパスがあったらそれ参照
    $path = $Args[0]
    $identifyPath = $Args[1]
} else {
    if (Test-Path env:SCOOP) {
        # SCOOP環境変数があったら、そっち参照
        $path = Join-Path (Join-Path ${env:SCOOP} "shims") "convert.exe"
        $identifyPath = Join-Path (Join-Path ${env:SCOOP} "shims") "identify.exe"
    } elseif (Test-Path (Join-Path(Join-Path(Join-Path ${env:USERPROFILE} "scoop") "shims") "convert.exe")) {
        # SCOOP標準設定
        $path = Join-Path(Join-Path(Join-Path ${env:USERPROFILE} "scoop") "shims") "convert.exe"
        $identifyPath = Join-Path(Join-Path(Join-Path ${env:USERPROFILE} "scoop") "shims") "identify.exe"
    } else {
        # 見つからない
        Write-Error "convert.exeの場所を解決できません。SCOOP環境変数に設定するか、第1引数にパスを指定してください"
        exit
    }
}

Get-ChildItem "*.JPG" -Name | ForEach-Object -Process {
    $file = $_
    $out = $file.Replace(".JPG", ".mini.JPG")
    Write-Output "$file -> $out"
    $rotate = 0
    if (Test-Path $identifyPath) {
        # identifyのパスがある
        $orientation = & $identifyPath -format "%[Exif:orientation]" $file
        Write-Output "orientation $orientation"
        
        if ($orientation -eq 3) {
            # 180°回転
            $rotate = 180
        } elseif ($orientation -eq 6) {
            # 90°回転
            $rotate = 90
        } elseif ($orientation -eq 8) {
            # 270°回転
            $rotate = 270
        }
    }
    & "${path}" $file -rotate $rotate -strip -geometry 50% -quality 90 $out
}