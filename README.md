# img4twitter

Twitter向けに写真を縮小するPowerShellスクリプト。

圧縮した写真をTwitterに投稿する機能はない。

## 依存関係
- powershell
- imagemagic

## usage
".JPG"(拡張子は大文字のみ固定)のファイルがあるディレクトリで以下のコマンドを実行
```powershell
img4twitter.ps1 [convert.exeのパス] [identify.exeのパス]
```
convert.exeのパスは指定しなかった場合、以下の順に探索する
- SCOOP環境変数以下のshimsディレクトリ
- scoopの標準インストール先({ユーザープロファイル}\scoop\shims)

拡張子を".mini.JPG"に変更したファイルに出力される
